package br.ufma.ecp.parser;

import br.ufma.ecp.parser.node.Node;
import br.ufma.ecp.parser.node.NodeParent;
import br.ufma.ecp.scanner.token.KeywordType;
import br.ufma.ecp.scanner.token.SymbolType;
import br.ufma.ecp.scanner.token.TokenType;

import java.util.Arrays;
import java.util.List;

import static br.ufma.ecp.parser.VMWriter.Command;
import static br.ufma.ecp.parser.VMWriter.Segment;

public class TermCompilers {
    public interface TermCompile {
        Node compile(Node.Eater eater);
    }

    public enum TermCompiler {
        STRING(TokenType.STRING, string()),
        NUMBER(TokenType.NUMBER, number()),
        IDENTIFIER(TokenType.IDENTIFIER, identifier()),
        TRUE(KeywordType.TRUE, booleans()),
        FALSE(KeywordType.FALSE, booleans()),
        NULL(KeywordType.NULL, booleans()),
        THIS(KeywordType.THIS, compileThis()),
        NEG(SymbolType.NOT, negative()),
        MINUS(SymbolType.MINUS, negative());

        private TermCompile termCompile;
        private Enum tokenType;

        TermCompiler(Enum tokenType, TermCompile termCompile) {
            this.termCompile = termCompile;
            this.tokenType = tokenType;
        }

        public Node process(Node.Eater eater) {
            return termCompile.compile(eater);
        }

        public static TermCompiler from(Enum lexeme) {
            return Arrays.stream(TermCompiler.values())
                    .filter(compiler -> compiler.tokenType.equals(lexeme))
                    .findFirst()
                    .orElse(null);
        }
    }

    public static TermCompile string() {
        return eater -> {
            var parser = eater.getParser();
            var vmWriter = parser.getVmWriter();
            var token = eater.eatReturn(TokenType.STRING);

            vmWriter.writePush(Segment.CONST, token.getLexeme().length());
            vmWriter.writeCall("String.new", 1);

            for (int i = 0; i < token.getLexeme().length(); i++) {
                vmWriter.writePush(Segment.CONST, token.getLexeme().getBytes()[i]);
                vmWriter.writeCall("String.appendChar", 2);
            }

            return NodeParent.term(eater.spit());
        };
    }

    public static TermCompile number() {
        return eater -> {
            var parser = eater.getParser();
            var vmWriter = parser.getVmWriter();

            eater.eat(TokenType.NUMBER);
            vmWriter.writePush(Segment.CONST, Integer.parseInt(eater.getLast().getToken().getLexeme()));
            return NodeParent.term(eater.hasAtLeast(1).spit());
        };
    }

    public static TermCompile identifier() {
        return eater -> {
            var parser = eater.getParser();
            var vmWriter = parser.getVmWriter();

            var token = eater.eatReturn(TokenType.IDENTIFIER);
            var symbol = parser.getSymbolTable().resolve(token.getLexeme());
            vmWriter.writePush(parser.kind2Segment(symbol.kind()), symbol.index());
            return NodeParent.term(eater.spit());
        };
    }

    public static TermCompile booleans() {
        return eater -> {
            var parser = eater.getParser();
            var vmWriter = parser.getVmWriter();
            var size = eater.spit().size();

            eater.eatBySubtypes(List.of(KeywordType.FALSE, KeywordType.NULL, KeywordType.TRUE));

            assert eater.spit().size() == size + 1;

            vmWriter.writePush(Segment.CONST, 0);
            if (eater.getLast().getToken().getSubType().equals(KeywordType.TRUE)) {
                vmWriter.writeArithmetic(Command.NOT);
            }
            return NodeParent.term(eater.spit());
        };
    }

    public static TermCompile compileThis() {
        return eater -> {
            var parser = eater.getParser();
            var vmWriter = parser.getVmWriter();

            eater.eat(KeywordType.THIS);
            vmWriter.writePush(Segment.POINTER, 0);
            return NodeParent.term(eater.spit());
        };
    }

    public static TermCompile negative() {
        return eater -> {
            var parser = eater.getParser();
            var vmWriter = parser.getVmWriter();

            var type = eater.getLast().getToken().getSubType();

            if (type.equals(SymbolType.MINUS)) {
                vmWriter.writeArithmetic(Command.NEG);
            } else {
                vmWriter.writeArithmetic(Command.NOT);
            }

            return NodeParent.term(eater.spit());
        };
    }


}
