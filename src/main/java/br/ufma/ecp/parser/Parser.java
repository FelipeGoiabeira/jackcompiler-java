package br.ufma.ecp.parser;

import br.ufma.ecp.parser.node.Node;
import br.ufma.ecp.parser.node.NodeParent;
import br.ufma.ecp.scanner.Scanner;
import br.ufma.ecp.scanner.token.*;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.function.Function;

import static br.ufma.ecp.parser.SymbolTable.Kind;
import static br.ufma.ecp.parser.TermCompilers.TermCompiler;
import static br.ufma.ecp.parser.VMWriter.Segment;

public class Parser {

    private Scanner scan;
    private Token currentToken;
    private Token peekToken;
    private String className;

    private VMWriter vmWriter = new VMWriter();
    private SymbolTable symbolTable = new SymbolTable();

    private Integer ifLabelNum = 0;
    private Integer whileLabelNum = 0;


    public static Map<SymbolType, VMWriter.Command> binOperators = Map.of(
            SymbolType.PLUS, VMWriter.Command.ADD,
            SymbolType.MINUS, VMWriter.Command.SUB,
            SymbolType.LT, VMWriter.Command.LT,
            SymbolType.GT, VMWriter.Command.GT,
            SymbolType.EQ, VMWriter.Command.EQ,
            SymbolType.AND, VMWriter.Command.AND,
            SymbolType.OR, VMWriter.Command.OR
    );

    public Parser(byte[] input) {
        scan = new Scanner(input);
        peekToken = scan.nextToken();
        nextToken();
    }

    public Token getCurrentToken() {
        return currentToken;
    }

    public Token getPeekToken() {
        return peekToken;
    }

    private void nextToken() {
        currentToken = peekToken;
        peekToken = scan.nextToken();
    }

    public Node parse() {
        if (currentToken instanceof KeywordToken) {
            var token = (KeywordToken) this.currentToken;
            return token.getSubType().process(this);
        }
        throw new Error("Keyword is expected, receive: " + currentToken.getLexeme());
    }

    public Token expectPeek(Enum type) {

        if (currentToken.getSubType() == type || currentToken.getType() == type) {
            var token = currentToken;
            nextToken();
            return token;
        }

        throw new Error("Syntax error - expected " + type + " found " + currentToken.getLexeme());

    }

    public static ParseProcessor expression() {
        return parser -> {

            Consumer<Node.Eater> eatNextTerm = eater -> {
                var operator = eater.eatReturn(TokenType.SYMBOL).getSubType();
                eater.eat(term());
                compileOperator(parser.getVmWriter(), operator);
            };

            var nodes = Node.eater(parser)
                    .eat(term())
                    .whileMatch(SymbolType::isOperator, eatNextTerm);

            return NodeParent.expression(nodes.spit());
        };
    }

    public static void compileOperator(VMWriter vmWriter, Enum operator) {
        if (operator.equals(SymbolType.ASTERISK)) {
            vmWriter.writeCall("Math.multiply", 2);
        } else if (operator.equals(SymbolType.SLASH)) {
            vmWriter.writeCall("Math.divide", 2);
        } else {
            vmWriter.writeArithmetic(Parser.binOperators.get(operator));
        }
    }

    public static Integer expressionList(Node.Eater eater) {
        var parser = eater.getParser();

        var varCount = new AtomicInteger();

        if (parser.getCurrentToken().getSubType().equals(SymbolType.RPAREN)) {
            eater.eat(NodeParent.expressionList(Collections.emptyList()));
            return 0;
        }

        Consumer<Node.Eater> eatExpression = neater -> {
            varCount.getAndIncrement();
            neater.eat(SymbolType.COMMA).eat(expression());
        };

        varCount.getAndIncrement();

        var nodes = Node.eater(parser)
                .eat(expression())
                .whileMatch(token -> token.equals(SymbolType.COMMA), eatExpression);

        eater.eat(NodeParent.expressionList(nodes.spit()));
        return varCount.get();
    }

    public static ParseProcessor term() {
        return parser -> {
            var vmWriter = parser.getVmWriter();

            Node.Eater eater = Node.eater(parser);
            eater.eatBySubtypes(List.of(SymbolType.MINUS, SymbolType.NOT));

            if (!eater.spit().isEmpty()) {

                var type = eater.getLast().getToken().getSubType();

                eater.eat(term());

                if (type.equals(SymbolType.MINUS)) {
                    vmWriter.writeArithmetic(VMWriter.Command.NEG);
                } else {
                    vmWriter.writeArithmetic(VMWriter.Command.NOT);
                }

                return NodeParent.term(eater.spit());

            }

            var current = parser.getCurrentToken();
            var peek = parser.getPeekToken();

            var isSubRoutine =
                    current.getType().equals(TokenType.IDENTIFIER) &&
                            (peek.getSubType().equals(SymbolType.LPAREN) || peek.getSubType().equals(SymbolType.DOT));

            if (isSubRoutine) {
                subRoutineCall(eater, parser);
                return NodeParent.term(eater.spit());
            }

            var isArray = current.getType().equals(TokenType.IDENTIFIER) &&
                    peek.getSubType().equals(SymbolType.LBRACKET);
            if (isArray) {
                var identifier = eater.eatReturn(TokenType.IDENTIFIER);

                var symbol = parser.getSymbolTable().resolve(identifier.getLexeme());

                eater.eat(SymbolType.LBRACKET)
                        .eat(expression());

                vmWriter.writePush(parser.kind2Segment(symbol.kind()), symbol.index());
                vmWriter.writeArithmetic(VMWriter.Command.ADD);

                eater.eat(SymbolType.RBRACKET);

                vmWriter.writePop(Segment.POINTER, 1);
                vmWriter.writePush(Segment.THAT, 0);

                return NodeParent.term(eater.spit());
            }

            var isExpression = current.getSubType().equals(SymbolType.LPAREN);
            if (isExpression) {
                eater.eat(SymbolType.LPAREN)
                        .eat(expression())
                        .eat(SymbolType.RPAREN);
                return NodeParent.term(eater.spit());
            }

            var nodesBySubType = Optional
                    .ofNullable(TermCompiler.from(parser.getCurrentToken().getSubType()))
                    .map(compiler -> compiler.process(eater));
            if (nodesBySubType.isPresent()) {
                return nodesBySubType.get();
            }

            var nodesByType = Optional
                    .ofNullable(TermCompiler.from(parser.getCurrentToken().getType()))
                    .map(compiler -> compiler.process(eater));
            if (nodesByType.isPresent()) {
                return nodesByType.get();
            }

            var nodes = eater.eatByTypes(
                    List.of(TokenType.IDENTIFIER, TokenType.STRING, TokenType.KEYWORD)
            );
            return NodeParent.term(nodes.spit());
        };
    }

    public static ParseProcessor let() {
        return parser -> {
            var vmWriter = parser.getVmWriter();

            var nodes = Node.eater(parser)
                    .eat(KeywordType.LET)
                    .eat(TokenType.IDENTIFIER);

            var symbol = parser.getSymbolTable().resolve(nodes.getLast().getToken().getLexeme());
            var isArray = false;
            while (parser.getCurrentToken().getSubType().equals(SymbolType.LBRACKET)) {
                nodes.eat(SymbolType.LBRACKET, expression(), SymbolType.RBRACKET);

                vmWriter.writePush(parser.kind2Segment(symbol.kind()), symbol.index());
                vmWriter.writeArithmetic(VMWriter.Command.ADD);

                isArray = true;
            }

            nodes.eat(SymbolType.EQ)
                    .eat(expression())
                    .eat(SymbolType.SEMICOLON);

            if (isArray)
            {
                vmWriter.writePop(Segment.TEMP, 0);    // push result back onto stack
                vmWriter.writePop(Segment.POINTER, 1); // pop address pointer into pointer 1
                vmWriter.writePush(Segment.TEMP, 0);   // push result back onto stack
                vmWriter.writePop(Segment.THAT, 0);    // Store right hand side evaluation in THAT 0.
            }
            else {


                vmWriter.writePop(
                        parser.kind2Segment(symbol.kind()),
                        symbol.index()
                );
            }

            return NodeParent.let(nodes.spit());
        };
    }

    public static ParseProcessor whileStatement() {
        return parser -> {

            var vmWriter = parser.getVmWriter();

            var whileLabelNum = parser.getWhileLabelNum();
            var labelTrue = "WHILE_EXP" + whileLabelNum;
            var labelFalse = "WHILE_END" + whileLabelNum;
            whileLabelNum++;
            parser.setWhileLabelNum(whileLabelNum);

            vmWriter.writeLabel(labelTrue);

            var eater = Node.eater(parser)
                    .eat(KeywordType.WHILE)
                    .eat(SymbolType.LPAREN)
                    .eat(expression())
                    .whileMatch(SymbolType::isOperator, weater -> weater.eat(expression()));

            vmWriter.writeArithmetic(VMWriter.Command.NOT);
            vmWriter.writeIf(labelFalse);

            eater.eat(SymbolType.RPAREN)
                    .eat(SymbolType.LBRACE)
                    .eat(statements());

            vmWriter.writeGoto(labelTrue);
            vmWriter.writeLabel(labelFalse);

            eater.eat(SymbolType.RBRACE);

            return NodeParent.whileStatement(eater.spit());
        };
    }

    public static void subRoutineCall(Node.Eater eater, Parser parser) {
        String funcName;
        var vmWriter = parser.getVmWriter();
        var nArgs = 0;

        var name = eater.eatReturn(TokenType.IDENTIFIER).getLexeme();

        if (parser.getCurrentToken().getSubType().equals(SymbolType.DOT)) {
            eater.eat(SymbolType.DOT)
                    .eat(TokenType.IDENTIFIER);
            funcName = eater.getLast().getToken().getLexeme();

            eater.eat(SymbolType.LPAREN);
            var symbol = parser.getSymbolTable().resolve(name);
            if (symbol != null) {
                vmWriter.writePush(parser.kind2Segment(symbol.kind()), symbol.index());
                funcName = symbol.type() + "." + funcName;

                nArgs = 1;
            } else {
                funcName = name + "." + funcName;
            }
            nArgs = nArgs + expressionList(eater);
            eater.eat(SymbolType.RPAREN);
            vmWriter.writeCall(funcName, nArgs);
            return;
        }

        // é um metodo da propria classe
        vmWriter.writePush(Segment.POINTER, 0);
        eater.eat(SymbolType.LPAREN);
        nArgs = expressionList(eater);
        nArgs++;

        eater.eat(SymbolType.RPAREN);

        funcName = parser.getClassName() + "." + name;
        vmWriter.writeCall(funcName, nArgs);
    }

    public static ParseProcessor doStatement() {
        return parser -> {

            var nodes = Node.eater(parser)
                    .eat(KeywordType.DO);

            subRoutineCall(nodes, parser);

            parser.getVmWriter().writePop(Segment.TEMP, 0);

            nodes.eat(SymbolType.SEMICOLON);

            return NodeParent.doStatement(nodes.spit());
        };
    }

    public static ParseProcessor returnStatement() {
        return parser -> {

            var nodes = Node.eater(parser)
                    .eat(KeywordType.RETURN);

            var vmWriter = parser.getVmWriter();

            if (parser.getCurrentToken().getSubType().equals(SymbolType.SEMICOLON)) {
                nodes.eat(SymbolType.SEMICOLON);
                vmWriter.writePush(Segment.CONST, 0);
                vmWriter.writeReturn();
                return NodeParent.returnStatement(nodes.spit());
            }

            nodes.eat(expression())
                    .eat(SymbolType.SEMICOLON);

            vmWriter.writeReturn();

            return NodeParent.returnStatement(nodes.spit());
        };
    }

    public static ParseProcessor ifStatement() {
        return parser -> {

            var vmWriter = parser.getVmWriter();
            var ifLabelNum = parser.getIfLabelNum();
            String labelTrue = "IF_TRUE" + ifLabelNum;
            String labelFalse = "IF_FALSE" + ifLabelNum;
            String labelEnd = "IF_END" + ifLabelNum;
            ifLabelNum++;
            parser.setIfLabelNum(ifLabelNum);

            var eater = Node.eater(parser)
                    .eat(KeywordType.IF)
                    .eat(SymbolType.LPAREN)
                    .eat(expression())
                    .whileMatch(SymbolType::isOperator, weater -> weater.eat(expression()))
                    .eat(SymbolType.RPAREN);

            vmWriter.writeIf(labelTrue);
            vmWriter.writeGoto(labelFalse);
            vmWriter.writeLabel(labelTrue);

            eater.eat(SymbolType.LBRACE)
                    .eat(statements())
                    .eat(SymbolType.RBRACE);

            if (parser.getCurrentToken().getSubType().equals(KeywordType.ELSE)) {
                vmWriter.writeGoto(labelEnd);
            }

            vmWriter.writeLabel(labelFalse);

            if (parser.getCurrentToken().getSubType().equals(KeywordType.ELSE)) {
                eater.eat(KeywordType.ELSE)
                        .eat(SymbolType.LBRACE)
                        .eat(statements())
                        .eat(SymbolType.RBRACE);

                vmWriter.writeLabel(labelEnd);
            }

            return NodeParent.ifStatement(eater.spit());
        };
    }

    public static ParseProcessor statements() {
        return parser -> {
            Consumer<Node.Eater> compileStatements = eater -> {
                var currentToken = parser.getCurrentToken();
                if (!(currentToken instanceof KeywordToken)) {
                    throw new Error("Keyword is expected, receive: " + currentToken.getLexeme());
                }
                var token = (KeywordToken) currentToken;
                eater.eat(token.getSubType().process(parser));
            };

            var nodes = Node.eater(parser)
                    .whileMatch(token -> !token.equals(SymbolType.RBRACE), compileStatements);

            return NodeParent.statements(nodes.spit());
        };
    }

    public static ParseProcessor classDec() {
        return parser -> {

            var eater = Node.eater(parser)
                    .eat(TokenType.KEYWORD);

            parser.setClassName(eater.eatReturn(TokenType.IDENTIFIER).getLexeme());
            Function<Enum, Boolean> endOfClassVarDec = token -> !token.equals(KeywordType.CONSTRUCTOR) &&
                    !token.equals(KeywordType.FUNCTION) && !token.equals(KeywordType.METHOD);

            eater.eat(SymbolType.LBRACE)
                    .whileMatch(endOfClassVarDec, neater -> neater.eat(classVarDec()))
                    .whileMatch(token -> !token.equals(SymbolType.RBRACE), neater -> neater.eat(subRoutineDec()))
                    .eat(SymbolType.RBRACE);

            return NodeParent.classDef(eater.spit());
        };
    }

    public static ParseProcessor parameterList() {
        return parser -> {

            var kind = Kind.ARG;

            if (parser.getCurrentToken().getSubType().equals(SymbolType.RPAREN)) {
                return NodeParent.parameterList(Collections.emptyList());
            }

            Consumer<Node.Eater> compileParameterList = eater -> {
                var size = eater.spit().size();
                eater
                        .eatByTypes(List.of(TokenType.IDENTIFIER))
                        .eatBySubtypes(List.of(KeywordType.VOID, KeywordType.INT, KeywordType.CHAR, KeywordType.BOOLEAN))
                        .hasAtLeast(size + 1);
                var type = eater.getLast().getToken().getSubType();
                var name = eater.eatReturn(TokenType.IDENTIFIER).getLexeme();
                parser.getSymbolTable().define(name, type.toString(), kind);
            };

            var nodes = Node.eater(parser);

            compileParameterList.accept(nodes);

            nodes.whileMatch(
                    token -> token.equals(SymbolType.COMMA),
                    eater -> {
                        eater.eat(SymbolType.COMMA);
                        compileParameterList.accept(eater);
                    }
            );

            return NodeParent.parameterList(nodes.spit());
        };
    }


    public static ParseProcessor subRoutineDec() {
        return parser -> {
            parser.getSymbolTable().startSubroutine();

            parser.setIfLabelNum(0);
            parser.setWhileLabelNum(0);

            var eater = Node.eater(parser)
                    .eatBySubtypes(List.of(KeywordType.CONSTRUCTOR, KeywordType.FUNCTION, KeywordType.METHOD))
                    .hasAtLeast(1);

            if(eater.getLast().getToken().getSubType().equals(KeywordType.METHOD)){
                parser.getSymbolTable().define("this", parser.getClassName(), Kind.ARG);
            }

            var type = eater.getLast().getToken().getSubType();
            eater
                    .eatByTypes(List.of(TokenType.IDENTIFIER))
                    .eatBySubtypes(List.of(KeywordType.VOID, KeywordType.INT, KeywordType.CHAR, KeywordType.BOOLEAN))
                    .hasAtLeast(2);

            var subRoutineName = parser.getClassName() + "." + eater.eatReturn(TokenType.IDENTIFIER).getLexeme();

            eater.eat(SymbolType.LPAREN)
                    .eat(parameterList())
                    .eat(SymbolType.RPAREN)
                    .eat(subRoutineBody(subRoutineName, type));


            return NodeParent.subRoutineDec(eater.spit());
        };
    }

    public static ParseProcessor subRoutineBody(String subRoutineName, Enum type) {
        return parser -> {

            if(subRoutineName.equals("Ball.setDestination")){
                System.out.println("parser = " + parser);
            }
            var vmWriter = parser.getVmWriter();

            var nodes = Node.eater(parser)
                    .eat(SymbolType.LBRACE);

            var hasVarDeclaration = parser.getCurrentToken().getSubType().equals(KeywordType.VAR);
            if (hasVarDeclaration) {
                nodes.eat(varDec())
                        .whileMatch(token -> token.equals(KeywordType.VAR), eater -> eater.eat(varDec()));
            }

            var varCount = parser.getSymbolTable().varCount(Kind.VAR);

            vmWriter.writeFunction(subRoutineName, varCount);

            if (type == KeywordType.CONSTRUCTOR) {
                varCount = parser.getSymbolTable().varCount(Kind.FIELD);
                vmWriter.writePush(Segment.CONST, varCount);
                vmWriter.writeCall("Memory.alloc", 1);
                vmWriter.writePop(Segment.POINTER, 0);
            } else if (type == KeywordType.METHOD) {
                vmWriter.writePush(Segment.ARG, 0);
                vmWriter.writePop(Segment.POINTER, 0);
            }

            nodes.eat(statements())
                    .eat(SymbolType.RBRACE);
            return NodeParent.subRoutineBody(nodes.spit());
        };
    }

    public static ParseProcessor varDec() {
        return parser -> {
            var kind = Kind.VAR;
            var eater = Node.eater(parser)
                    .eat(KeywordType.VAR)
                    .eatByTypes(List.of(TokenType.IDENTIFIER))
                    .eatBySubtypes(List.of(KeywordType.VOID, KeywordType.INT, KeywordType.CHAR, KeywordType.BOOLEAN))
                    .hasAtLeast(2);

            var type = eater.getLast().getToken().getLexeme();


            Consumer<Node.Eater> consumeVarDec = ceater -> {
                var name = ceater.eatReturn(TokenType.IDENTIFIER).getLexeme();
                parser.getSymbolTable().define(name, type, kind);
            };

            consumeVarDec.accept(eater);

            eater.whileMatch(SymbolType.COMMA, weater -> {
                weater.eat(SymbolType.COMMA);
                consumeVarDec.accept(weater);
            });

            eater.eat(SymbolType.SEMICOLON);

            return NodeParent.varDec(eater.spit());
        };
    }

    public static ParseProcessor classVarDec() {
        return parser -> {

            var eater = Node.eater(parser)
                    .eatBySubtypes(List.of(KeywordType.FIELD, KeywordType.STATIC))
                    .hasAtLeast(1);

            var variable = eater.getLast().getToken();
            var kind = variable.getSubType().equals(KeywordType.FIELD) ? Kind.FIELD : Kind.STATIC;

            eater.eatByTypes(List.of(TokenType.IDENTIFIER))
                    .eatBySubtypes(List.of(KeywordType.VOID, KeywordType.INT, KeywordType.CHAR, KeywordType.BOOLEAN))
                    .hasAtLeast(2);

            var type = eater.getLast().getToken().getLexeme();

            var name = eater.eatReturn(TokenType.IDENTIFIER).getLexeme();

            var symbolTable = parser.getSymbolTable();
            symbolTable.define(name, type, kind);

            eater.whileMatch(token -> token.equals(SymbolType.COMMA), neater -> {
                        neater.eat(SymbolType.COMMA).eat(TokenType.IDENTIFIER);
                        symbolTable.define(neater.getLast().getToken().getLexeme(), type, kind);
                    })
                    .eat(SymbolType.SEMICOLON);

            return NodeParent.classVarDec(eater.spit());
        };
    }

    public static ParseProcessor defaultParser() {
        return parser -> {
            throw new Error("Not implemented");
        };
    }

    public VMWriter getVmWriter() {
        return vmWriter;
    }

    public SymbolTable getSymbolTable() {
        return symbolTable;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String VMOutput() {
        return vmWriter.vmOutput();
    }

    public Integer getIfLabelNum() {
        return ifLabelNum;
    }

    public void setIfLabelNum(Integer ifLabelNum) {
        this.ifLabelNum = ifLabelNum;
    }

    public Integer getWhileLabelNum() {
        return whileLabelNum;
    }

    public void setWhileLabelNum(Integer whileLabelNum) {
        this.whileLabelNum = whileLabelNum;
    }

    public Segment kind2Segment(Kind kind) {
        if (kind == Kind.STATIC) return Segment.STATIC;
        if (kind == Kind.FIELD){
            return Segment.THIS;
        }
        if (kind == Kind.VAR) return Segment.LOCAL;
        if (kind == Kind.ARG) return Segment.ARG;
        return null;
    }
}
