package br.ufma.ecp;

import br.ufma.ecp.parser.Parser;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class ParserTest extends TestSupport {

    @Test
    void testParserSquareGame() throws IOException {
        var input = fromFile("parser/SquareGame.jack");
        var expectedResult = fromFile("parser/SquareGame.xml");
        var parser = new Parser(input.getBytes(StandardCharsets.UTF_8));
        var node = parser.parse();

        Assertions.assertEquals(expectedResult, node.toString()+ "\r\n");
        System.out.println(node);
    }

    @Test
    void testParserSquare() throws IOException {
        var input = fromFile("parser/Square.jack");
        var expectedResult = fromFile("parser/Square.xml");
        var parser = new Parser(input.getBytes(StandardCharsets.UTF_8));
        var node = parser.parse();

        Assertions.assertEquals(expectedResult, node.toString()+ "\r\n");
        System.out.println(node);
    }
}
